import { InputHTMLAttributes, ReactNode } from 'react';
import { Slot } from '@radix-ui/react-slot';
import { clsx } from 'clsx'

export interface InputFieldProps extends InputHTMLAttributes<HTMLInputElement> { }

export interface InputRootProps {
  children: ReactNode;
}
function InputRoot(props: InputRootProps) {
  return <div className='flex items-center gap-3 h-12 py-4 px-3 rounded bg-gray-800 focus-within:ring-2 ring-cyan-300'>
    {props.children}
  </div>
}


export interface InputIconProps {
  children: ReactNode;
}
function InputIcon(props: InputIconProps) {
  return <Slot className='w-5 h-6 text-gray-400'>
    {props.children}
  </Slot>
}


export function InputText(props: InputFieldProps) {
  return (
    <input
      className="bg-transparent flex-1  text-gray-100 text-xs outline-none  placeholder:text-gray-400 "
      {...props} />
  )
}

export const InputField = {
  Root: InputRoot,
  Input: InputText,
  Icon: InputIcon,
}