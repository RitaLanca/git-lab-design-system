import { Meta, StoryObj } from '@storybook/react'
import { Text, TextProps } from "./Text";

export default {
  title: 'Components/Text',
  component: Text,
  args: {
    children: 'Lorem ipsum',
    size: 'md',
    asChild: false,
  },
  // argTypes: {
  //   size: {
  //     options: ['sm', 'md', 'lg'],
  //     control: {
  //       type: 'inline-radio'
  //     }
  //   }
  // }
} as Meta<TextProps>;

export const Default: StoryObj<TextProps> = {}

export const Smalll: StoryObj<TextProps> = {
  args: {
    size: 'sm'
  },

}
export const Large: StoryObj<TextProps> = {
  args: {
    size: 'lg'
  }
}

export const Custom: StoryObj<TextProps> = {
  args: {
    size: 'lg',
    asChild: true,
    children: (<p>paragrafo</p>)
  },
  argTypes: {
    children: {
      table: {
        disabled: true,
      }
    },
    asChild: {
      table: {
        disabled: true,
      }
    }
  }

}