import { Meta, StoryObj } from '@storybook/react'
import { Envelope } from 'phosphor-react';
import { InputField, InputRootProps } from "./InputField";

export default {
  title: 'Components/InputField',
  component: InputField.Root,
  args: {
    children: [
      <InputField.Icon><Envelope /></InputField.Icon>,
      <InputField.Input placeholder='Type your email address' />
    ]
  },
  argTypes: {
    children: {
      table: {
        disabled: true,
      }
    }
  }

} as Meta<InputRootProps>;

export const Default: StoryObj<InputRootProps> = {}

export const InputWithoutIcon: StoryObj<InputRootProps> = {
  args: {
    children: [
      <InputField.Input placeholder='Type your email address' />
    ]
  },
  argTypes: {
    children: {
      table: {
        disabled: true,
      }
    }
  }
}
