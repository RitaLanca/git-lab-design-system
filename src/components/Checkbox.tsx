import { ReactNode } from 'react';
import * as CheckboxLib from '@radix-ui/react-checkbox';
import { Check } from 'phosphor-react'
import { InputText } from './InputField';

export interface CheckboxProps { }

export function Checkbox({ }: CheckboxProps) {
  return (
    <CheckboxLib.Root className='w-6 h-5 p-[2px] bg-gray-800 rounded' >
      <CheckboxLib.Indicator asChild>
        <Check weight='bold' className='h-5 w-5 text-cyan-500' />
      </CheckboxLib.Indicator>
    </CheckboxLib.Root>
  )
}